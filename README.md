# State of Europe web app

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Please install the dependencies first:

```shell
npm install
```

To start the app in development mode, run the following command:

```shell
npm start
```

It will open [http://localhost:3000](http://localhost:3000) in your browser.
