// react
import React, { Component } from 'react';

// core
import {
  ComposableMap,
  ZoomableGroup,
  Geographies,
  Geography,
} from 'react-simple-maps';

// other
import { worldCountries } from '../assets/data/worldCountries';

// styles
import { composableMapStyle, geographyStyle } from '../assets/jss/reactSimpleMapsStyle';

class Map extends Component {
  render() {
    return (
      <ComposableMap
        projectionConfig={{ width: 980, scale: 660, yOffset: 295, xOffset: -100 }}
        style={ composableMapStyle }
      >
        <ZoomableGroup>
        <Geographies geography={ worldCountries }>
          {(geographies, projection) =>
            geographies.map((geography, i) =>
              geography.properties.continent === "Europe" && (
            <Geography
              key={ i }
              geography={ geography }
              projection={ projection }
              style={ geographyStyle }
              onClick={ () => this.props.handleRouteChange(geography.properties.admin) }
              />
          ))}
        </Geographies>
        </ZoomableGroup>
      </ComposableMap>
    );
  }
}

export default Map;
