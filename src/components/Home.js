//react
import React, { Component } from 'react';

//core
import withStyles from "@material-ui/core/styles/withStyles";

// styles
import '../App.css';
import dashboardStyle from "../assets/jss/material-dashboard-react/dashboardStyle.jsx";

// components
import Map from './Map';
import Header from './Header';


class Home extends Component {
  render() {
    const { classes } = this.props;

    return (
      <div>
        <Header
          title="Hello world!"
          text="This is Europe today. Click on a country to see all kinds of awesome data about its population."
        />
        <div className={classes.mapWrapper}>
          <Map handleRouteChange={this.props.handleRouteChange} />
        </div>
      </div>
    );
  }
}

export default withStyles(dashboardStyle)(Home);
