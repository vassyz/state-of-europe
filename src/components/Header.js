// react
import React, { Component } from 'react';

// core
import withStyles from "@material-ui/core/styles/withStyles";

// styles
import '../App.css';
import dashboardStyle from "../assets/jss/material-dashboard-react/dashboardStyle.jsx";

// components
import Card from "./presentational/Card.jsx";
import CardBody from "./presentational/CardBody.jsx";
import CardFooter from "./presentational/CardFooter.jsx";

class Header extends Component {
  render() {
    const { classes, title, text } = this.props;

    return (
      <Card>
        <CardBody>
        <h1 className={classes.cardTitle}>{title}</h1>
        <p className={classes.cardText}>{text}</p>
        </CardBody>
        <CardFooter stats>
          <div className={classes.stats}>
            <a href="/" className={classes.cardTextLink}>
              Home
            </a>
          </div>
        </CardFooter>
      </Card>
    );
  }
}

export default withStyles(dashboardStyle)(Header);
