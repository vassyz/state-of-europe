// react
import React, { Component } from 'react';

// core
import { Route, withRouter, Switch } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Grid from "@material-ui/core/Grid";

// styles
import appStyle from "../assets/jss/material-dashboard-react/components/appStyle.jsx";

// components
import Sidebar from './Sidebar';
import Home from './Home';
import PopulationContainer from './containers/PopulationContainer';
import GridItem from "./presentational/GridItem.jsx";

// others
import { getPopulation } from '../actions'
import logo from "../assets/img/reactlogo.png";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes } = this.props;

    return (

      <div className={classes.wrapper}>
        <div className={classes.mainPanel} ref="mainPanel">
          <Sidebar
            logoText={"State of Europe"}
            logo={logo}
          ></Sidebar>
          <Grid container>
            <GridItem xs={12} sm={12} md={12}>
              <Switch>
                <Route
                  path={`/country/:countryId`}
                  render={(props) => <PopulationContainer {...props} handleRouteChange={this.handleRouteChange} />}
                />
                <Route
                  path="/"
                  render={(props) => <Home {...props} handleRouteChange={this.handleRouteChange} />}
                />
              </Switch>
            </GridItem>
          </Grid>
        </div>
      </div>
    );
  }

  handleRouteChange = (geography) => {
    this.props.history.push(`/country/${geography}`);
    this.props.store.dispatch(getPopulation(geography));
  }
}

export default withRouter(withStyles(appStyle)(App));
