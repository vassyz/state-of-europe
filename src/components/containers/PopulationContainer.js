// react
import React, { Component } from 'react';

// core
import { connect } from 'react-redux'

// components
import Population from '../Population'
import Header from '../Header'

// other
import { getPopulation } from '../../actions'

class PopulationContainer extends Component {
  componentWillMount() {
    this.props.handleRouteChange(this.props.match.params.countryId);
  }

  render() {
    const country = this.props.match.params.countryId;
    const title = `Hello ${country}!`;
    const text = `This is ${country}'s population today, split by gender and age groups. Dig deeper for life expectancy and other relevant information for the selected age/gender group.`;

    return(
      <div>
        <Header
          title={title}
          text={text}
        />
        <Population population={this.props.population} />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  getPopulation
})

const mapStateToProps = state => ({
  population: state.population
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PopulationContainer)