import React from "react";
import PropTypes from "prop-types";
import { Link } from 'react-router-dom';

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Drawer from "@material-ui/core/Drawer";

// styles
import sidebarStyle from "../assets/jss/material-dashboard-react/components/sidebarStyle.jsx";

class Sidebar extends React.Component {
  render() {
    const { classes, logo, logoText } = this.props; 

    const brand = (
      <div className={classes.logo}>
        <Link to="/" className={classes.logoLink}>
          <div className={classes.logoImage}>
            <img src={logo} alt="logo" className={classes.img} />
          </div>
          {logoText}
        </Link>
      </div>
    );

    return (<Drawer
        variant="persistent"
        anchor="right"
        open={true}
        classes={{
          paper: classes.drawerPaper
        }}
      >
    {brand}
    <div className={classes.sidebarWrapper}>
      <div className={classes.sidebarSlogan}>
        <p>Welcome to The State of Europe. This app is built in <a href="https://reactjs.org/" target="_blank" rel="noopener noreferrer" className={classes.sidebarSloganLink}>React</a>, using <a href="https://github.com/facebook/create-react-app" target="_blank" rel="noopener noreferrer" className={classes.sidebarSloganLink}>create-react-app</a>, <a href="https://www.react-simple-maps.io/" target="_blank" rel="noopener noreferrer" className={classes.sidebarSloganLink}>react-simple-maps</a> and <a href="https://material-ui.com/" target="_blank" rel="noopener noreferrer" className={classes.sidebarSloganLink}>material-ui</a>. It consumes the <a href="http://api.population.io/" target="_blank" rel="noopener noreferrer" className={classes.sidebarSloganLink}>World Population API</a>.</p>
      </div>
    </div>
    <div className={classes.background}/>
    </Drawer>);
  }
}

Sidebar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(sidebarStyle)(Sidebar);
