// react
import React, { Component } from 'react';

// core
import { connect } from 'react-redux'
// import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

class Population extends Component {
  render() {
    return (
      <div>
        {this.props.population.length ? (
          <Paper>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Age</TableCell>
                  <TableCell numeric>Female Population</TableCell>
                  <TableCell numeric>Male Population</TableCell>
                  <TableCell numeric>Total</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {this.props.population.map(row => {
                  return (
                    <TableRow key={row.age}>
                      <TableCell>{row.age}</TableCell>
                      <TableCell numeric>{row.females}</TableCell>
                      <TableCell numeric>{row.males}</TableCell>
                      <TableCell numeric>{row.total}</TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </Paper>
        ) : (
          <h2>No details found for the current country.</h2>
        )}
      </div>
    );
  }
}

export default connect()(Population);