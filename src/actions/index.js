import { actionTypes as types, urls } from '../constants';
import { get } from '../helpers';

export const getPopulation = (country) => (dispatch, getState) => {
  dispatch({type: types.POPULATION_REQUEST});
  get({
    url: `${urls.POPULATION}/${country}`,
    success: types.POPULATION_SUCCESS,
    failure: types.POPULATION_FAILURE,
    dispatch,
  });
};