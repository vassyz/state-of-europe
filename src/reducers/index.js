import { combineReducers } from 'redux';

import population from './population';

const rootReducer = combineReducers({
  population,
});

export default rootReducer;