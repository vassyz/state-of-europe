import { actionTypes as types } from '../constants';

const population = (state = {}, action) => {
  switch (action.type) {
    case types.POPULATION_SUCCESS:
      return action.data;
    case types.POPULATION_FAILURE:
      return {};
    default:
      return state;
  }
};

export default population;