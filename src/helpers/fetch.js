export const get = async ({url, success, failure, dispatch}) => {
  try {
    const res = await fetch(url, {method: 'GET', mode: 'cors'});
    const data = await res.json();
    if (res.status === 200) {
      dispatch({type: success, data});
    } else {
      dispatch({type: failure});
    }
  } catch (e) {
    dispatch({type: failure});
  }
};