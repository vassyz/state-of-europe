const composableMapStyle = {
  width: "100%",
  height: "100%"
};

const geographyStyle = {
  default: {
    fill: "#d7f3f7",
    stroke: "#47acb8",
    strokeWidth: 0.75,
    outline: "none",
  },
  hover: {
    fill: "#94d4dc",
    stroke: "#47acb8",
    strokeWidth: 0.75,
    outline: "none",
  },
  pressed: {
    fill: "#94d4dc",
    stroke: "#47acb8",
    strokeWidth: 0.75,
    outline: "none",
  }
};

export { 
  composableMapStyle,
  geographyStyle
};