export const appStyle = {
  root: {
    flexGrow: 1,
  },
  flex: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  paper: {
    width: '100%',
    maxWidth: 980,
    margin: '20px auto',
    border: '1px solid #607D8B',
  },
};