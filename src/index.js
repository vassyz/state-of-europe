// react
import React from 'react';
import ReactDOM from 'react-dom';

// core
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

// components
import App from './components/App';

// other
import { store } from './store';

export const Root = ({store}) => (
  <Provider store={store}>
    <BrowserRouter>
      <App store={store}/>
    </BrowserRouter>
  </Provider>
);

ReactDOM.render(<Root store={store}/>, document.getElementById('root'));